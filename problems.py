import sys


def main():
  problem1()
  problem2()
  problem3()
  problem4()
  problem5()
  problem6()
  problem7()

def problem1():
    name = input()
    name = dict.fromkeys(name)

    if len(name) % 2 == 0:
        print("CHAT WITH HER!")
    else:
        print("IGNORE HIM!")


def problem2():
    ret = 0
    prev = ''
    for _ in range(int(input())):
        curr = input()
        if curr != prev:
            ret += 1
            prev = curr
    print(ret)

def problem3():
    def get_list():
        return list(map(int, sys.stdin.readline().strip().split()))

    tests = int(input())
    while tests != 0:
        inp = input()
        if inp.strip() == '':
            continue
        size, limit = [int(x) for x in inp.split()]
        p = get_list()
        q = get_list()
        for i in range(size):
            if p[i] + q[size - i - 1] > limit:
                print("No")
                break
        else:
            print("Yes")
        tests = tests - 1

def problem4():
    counter = 0
    n = input()
    
    while counter < int(n):
        word = input()
        word_length = len(word)
        if word_length > 10:
            abbreviate = str(word[0]) + str(word_length - 2) + str(word[word_length - 1])
            print(abbreviate)
        else:
            print(word)
        counter += 1

def problem5():
    x=input()
    y=len(x)
    count=x.count('a')
    if  (y//2)+1>count:
        y=(count*2)-1
    print(y)

def problem6():
    a = input().lower() 
    b = input().lower()
 
    if a > b:
        print(1)
    elif a < b:
        print(-1)
    else:
        print(0)


def problem7():
    start = convert(input())
    start_totalsecs = start.tm_sec + start.tm_min*60 + start.tm_hour*3600
    end = convert(input())
    end_totalsecs = end.tm_sec + end.tm_min*60 + end.tm_hour*3600
    total = (start_totalsecs + end_totalsecs)/ 2
    print(sec_to_time(total))


def convert(input):
    time_object = time.strptime(input, '%H:%M')
    return time_object
    #print(str(my_time.tm_hour) + ":" + str(my_time.tm_min))

def sec_to_time(seconds):
    seconds = seconds % (24 * 3600)
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
      
    return "%02d:%02d" % (hour, minutes)
    
main()
